package models

import (
	"gorm.io/datatypes"
	"time"
)

type AdvokasiProgress struct {
	Id         uint           `json:"id" gorm:"primaryKey"`
	CreatedAt  time.Time      `json:"created_at"`
	UpdatedAt  time.Time      `json:"updated_at"`
	AdvokasiId uint           `json:"advokasi_id"`
	Advokasi   Advokasi       `json:"advokasi" gorm:"foreignKey:AdvokasiId;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	Laporan    string         `json:"laporan"`
	Tanggal    datatypes.Date `json:"tanggal"`
	Path       string         `json:"path"`
}

func (a AdvokasiProgress) TableName() string {
	return "bispro.advokasi_progress"
}
