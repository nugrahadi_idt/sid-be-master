package models

import (
	"time"
)

type Kecamatan struct {
	Id        uint           `json:"id" gorm:"primaryKey"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	Code      string         `json:"code" gorm:"unique"`
	Name      string         `json:"name"`
	KabkotaId uint           `json:"kabkota_id"`
	Kabkota   Kabkota        `json:"kabkota" gorm:"foreignKey:KabkotaId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}

func (k Kecamatan) TableName() string {
	return "master_ref.kecamatan"
}
