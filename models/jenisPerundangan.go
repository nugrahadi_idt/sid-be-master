package models

import (
	"time"
)

type JenisPerundangan struct {
	Id        uint           `json:"id" gorm:"primaryKey"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	Name      string         `json:"name" gorm:"unique"`
	Icon      string         `json:"icon"`
}

func (j JenisPerundangan) TableName() string {
	return "master_ref.jenis_perundangan"
}
