package models

import (
	"time"
)

type Subdirektorat struct {
	Id           uint           `json:"id" gorm:"primaryKey"`
	CreatedAt    time.Time      `json:"created_at"`
	UpdatedAt    time.Time      `json:"updated_at"`
	Code         string         `json:"code" gorm:"unique"`
	Name         string         `json:"name"`
	DirektoratId uint           `json:"direktorat_id"`
	Direktorat   Direktorat     `json:"direktorat" gorm:"foreignKey:DirektoratId;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
}

func (s Subdirektorat) TableName() string {
	return "master_ref.subdirektorat"
}
