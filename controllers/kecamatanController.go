package controllers

import (
	"github.com/gofiber/fiber/v2"
	"sidat-amws/database"
	"sidat-amws/models"
	"strconv"
)

func AllKecamatan(c *fiber.Ctx) error {
	var kecamatans []models.Kecamatan

	database.DB.Preload("Kabkota").Find(&kecamatans)

	return c.JSON(kecamatans)
}

func CreateKecamatan(c *fiber.Ctx) error {
	var kecamatan models.Kecamatan

	if err := c.BodyParser(&kecamatan); err != nil {
		return err
	}

	database.DB.Create(&kecamatan)

	return c.JSON(kecamatan)
}

func GetKecamatan(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	kecamatan := models.Kecamatan{Id: uint(id)}

	database.DB.Preload("Kabkota").Find(&kecamatan)

	return c.JSON(kecamatan)
}

func UpdateKecamatan(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	kecamatan := models.Kecamatan{Id: uint(id)}

	if err := c.BodyParser(&kecamatan); err != nil {
		return err
	}

	database.DB.Model(&kecamatan).Updates(&kecamatan)

	return c.JSON(kecamatan)
}

func DeleteKecamatan(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	kecamatan := models.Kecamatan{Id: uint(id)}

	database.DB.Delete(&kecamatan)

	c.Status(fiber.StatusNoContent)
	return c.JSON(fiber.Map{
		"message": "kecamatan deleted",
	})
}
