package controllers

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
	"math"
	"net/url"
	"sidat-amws/database"
	"sidat-amws/models"
	"sidat-amws/models/reqresp"
	"sidat-amws/util"
	"strconv"
	"strings"
)

// AllSubdirektorat Subdirektorat list
// @Summary Subdirektorat list
// @Description Subdirektorat list
// @Tags Master
// @Accept json
// @Produce json
// @param direktorat_id query int false "Direktorat ID"
// @param nama query string false "Name to search"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /master/subdirektorat [get]
func AllSubdirektorat(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	direktoratId, _ := strconv.Atoi(c.Query("direktorat_id", "0"))
	sortStr := c.Query("sort", "")
	nama := c.Query("nama", "")

	var records []models.Subdirektorat

	baseQ := database.DB.Preload(clause.Associations)
	if direktoratId>0 {
		baseQ = baseQ.Where("direktorat_id = ?", direktoratId)
	}
	if nama != "" {
		baseQ = baseQ.Where("UPPER(name) LIKE ?", "%"+ strings.ToUpper(nama) +"%")
	}

	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	baseQ.Offset(offset).Limit(limit).Find(&records)

	baseQ.Model(&models.Subdirektorat{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       records,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateSubdirektorat Create a Subdirektorat
// @Summary Create a Subdirektorat
// @Description Create a Subdirektorat
// @Tags Master
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param data body models.Subdirektorat true "Subdirektorat data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /master/subdirektorat [post]
func CreateSubdirektorat(c *fiber.Ctx) error {
	var record models.Subdirektorat

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Preload(clause.Associations).Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "subdirektorat", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetSubdirektorat Get single Subdirektorat data
// @Summary Get single Subdirektorat data
// @Description Get single Subdirektorat data
// @Tags Master
// @Accept json
// @Produce json
// @param id path int true "Subdirektorat ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /master/subdirektorat/{id} [get]
func GetSubdirektorat(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Subdirektorat{Id: uint(id)}

	result := database.DB.Preload(clause.Associations).Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting a record",
		Data:    record,
	})
}

// UpdateSubdirektorat Update a Subdirektorat
// @Summary Update a Subdirektorat
// @Description Update a Subdirektorat
// @Tags Master
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Subdirektorat ID"
// @param data body models.Subdirektorat true "Subdirektorat data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /master/subdirektorat/{id} [put]
func UpdateSubdirektorat(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Subdirektorat{Id: uint(id)}

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	var oldData models.Subdirektorat
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Model(&record).Preload(clause.Associations).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "subdirektorat", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteSubdirektorat Delete a Subdirektorat
// @Summary Delete a Subdirektorat
// @Description Delete a Subdirektorat
// @Tags Master
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Subdirektorat ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /master/subdirektorat/{id} [delete]
func DeleteSubdirektorat(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Subdirektorat{Id: uint(id)}

	var oldData models.Subdirektorat
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "subdirektorat", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Subdirektorat record deleted",
		Data:    record,
	})
}
