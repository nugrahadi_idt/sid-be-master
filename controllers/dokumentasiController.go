package controllers

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
	"math"
	"net/url"
	"os"
	"sidat-amws/database"
	"sidat-amws/models"
	"sidat-amws/models/reqresp"
	"sidat-amws/util"
	"strconv"
	"strings"
)

// AllDokumentasi Dokumentasi list
// @Summary Dokumentasi list
// @Description Dokumentasi list
// @Tags Dokumentasi
// @Accept json
// @Produce json
// @param jenis_perundangan_id query int false "Jenis Perundangan ID"
// @param nama query string false "Nama Dokumentasi"
// @param sort query string false "Sort by"
// @param tahun query int false "Tahun Dokumentasi"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /dokumentasi [get]
func AllDokumentasi(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	jenisPerundanganId, _ := strconv.Atoi(c.Query("jenis_perundangan_id", "0"))
	nama := c.Query("nama", "")
	tahun, _ := strconv.Atoi(c.Query("tahun", "0"))
	sortStr := c.Query("sort", "")


	var records []models.Dokumentasi

	baseQ := database.DB.Preload(clause.Associations)
	if jenisPerundanganId > 0 {
		baseQ = baseQ.Where("jenis_perundangan_id = ?", jenisPerundanganId)
	}

	if nama != "" {
		baseQ = baseQ.Where("UPPER(nama) LIKE ?", "%"+strings.ToUpper(nama)+"%")
	}

	if tahun > 0 {
		baseQ = baseQ.Where("tahun = ?", tahun)
	}

	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	//execute query
	baseQ.Offset(offset).Limit(limit).Find(&records)

	baseQ.Model(&models.Dokumentasi{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       records,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// GetDokumentasiStats Dokumentasi stats
// @Summary Dokumentasi stats
// @Description Dokumentasi stats
// @Tags Dokumentasi
// @Accept json
// @Produce json
// @Success 200 {object} reqresp.SuccessResponse
// @Router /dokumentasi/stats [get]
func GetDokumentasiStats(c *fiber.Ctx) error {

	var records []reqresp.StatsResponse

	baseQ := database.DB.Table("dokumentasi k").Select("jp.name as name, count(*) as total")
	baseQ = baseQ.Joins("join jenis_perundangan jp on jp.id = k.jenis_perundangan_id").Group("jp.name")

	//execute query
	baseQ.Find(&records)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success calculating Dokumentasi statistic",
		Data:    records,
	})
}

// GetDokumentasiIncreaseView Increase document view count
// @Summary Increase document view count
// @Description Increase document view count
// @Tags Dokumentasi
// @Accept json
// @Produce json
// @param id path int true "Dokumentasi ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Router /dokumentasi/increase-view/{id} [get]
func GetDokumentasiIncreaseView(c *fiber.Ctx) error {

	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Dokumentasi{Id: uint(id)}

	result := database.DB.Preload(clause.Associations).Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	record.TotalView += 1

	result = database.DB.Model(&record).Preload(clause.Associations).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success increasing view count",
		Data:    record,
	})
}

// GetDokumentasiIncreaseDownload Increase document download count
// @Summary Increase document download count
// @Description Increase document download count
// @Tags Dokumentasi
// @Accept json
// @Produce json
// @param id path int true "Dokumentasi ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Router /dokumentasi/increase-download/{id} [get]
func GetDokumentasiIncreaseDownload(c *fiber.Ctx) error {

	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Dokumentasi{Id: uint(id)}

	result := database.DB.Preload(clause.Associations).Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	record.TotalDownload += 1

	result = database.DB.Model(&record).Preload(clause.Associations).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success increasing download count",
		Data:    record,
	})
}

// CreateDokumentasi Create a Dokumentasi
// @Summary Create a Dokumentasi
// @Description Create a Dokumentasi
// @Tags Dokumentasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param data body models.Dokumentasi true "Dokumentasi data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /dokumentasi [post]
func CreateDokumentasi(c *fiber.Ctx) error {
	var record models.Dokumentasi

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Preload(clause.Associations).Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "dokumentasi", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetDokumentasi Get single Dokumentasi data
// @Summary Get single Dokumentasi data
// @Description Get single Dokumentasi data
// @Tags Dokumentasi
// @Accept json
// @Produce json
// @param id path int true "Dokumentasi ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /dokumentasi/{id} [get]
func GetDokumentasi(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Dokumentasi{Id: uint(id)}

	result := database.DB.Preload(clause.Associations).Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting a record",
		Data:    record,
	})
}

// UpdateDokumentasi Update a Dokumentasi
// @Summary Update a Dokumentasi
// @Description Update a Dokumentasi
// @Tags Dokumentasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Dokumentasi ID"
// @param data body models.Dokumentasi true "Dokumentasi data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /dokumentasi/{id} [put]
func UpdateDokumentasi(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Dokumentasi{Id: uint(id)}

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	var oldData models.Dokumentasi
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Model(&record).Preload(clause.Associations).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "dokumentasi", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteDokumentasi Delete a Dokumentasi
// @Summary Delete a Dokumentasi
// @Description Delete a Dokumentasi
// @Tags Dokumentasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Dokumentasi ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /dokumentasi/{id} [delete]
func DeleteDokumentasi(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Dokumentasi{Id: uint(id)}

	var oldData models.Dokumentasi
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// delete uploaded file
	paths := strings.Split(record.ProdukPath, ";")
	for _, path := range paths {
		_ = os.Remove("./public/" + path)
	}
	//if err != nil {
	//	return c.Status(fiber.StatusInternalServerError).JSON(err)
	//}

	// send to log
	util.SendToAudit(c, "dokumentasi", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Dokumentasi record deleted",
		Data:    record,
	})
}
