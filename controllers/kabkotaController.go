package controllers

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
	"math"
	"net/url"
	"sidat-amws/database"
	"sidat-amws/models"
	"sidat-amws/models/reqresp"
	"sidat-amws/util"
	"strconv"
	"strings"
)

// AllKabkota Kabkota list
// @Summary Kabkota list
// @Description Kabkota list
// @Tags Reference
// @Accept json
// @Produce json
// @param nama query string false "Name to search"
// @param provinsi_id query int false "Provinsi ID"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /ref/kabkota [get]
func AllKabkota(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	var records []models.Kabkota

	nama := c.Query("nama", "")
	provinsiId, _ := strconv.Atoi( c.Query("provinsi_id", "0"))
	sortStr := c.Query("sort", "")

	baseQ := database.DB.Preload(clause.Associations)
	if nama != "" {
		baseQ = baseQ.Where("UPPER(name) LIKE ?", "%"+strings.ToUpper(nama)+"%")
	}
	if provinsiId >0 {
		baseQ = baseQ.Where("provinsi_id = ?", provinsiId)
	}
	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}
	baseQ.Offset(offset).Limit(limit).Find(&records)

	baseQ.Model(&models.Kabkota{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       records,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateKabkota Create a Kabkota
// @Summary Create a Kabkota
// @Description Create a Kabkota
// @Tags Reference
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param data body models.Kabkota true "Kabkota data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /ref/kabkota [post]
func CreateKabkota(c *fiber.Ctx) error {
	var record models.Kabkota

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Preload(clause.Associations).Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "kabkota", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetKabkota Get single Kabkota data
// @Summary Get single Kabkota data
// @Description Get single Kabkota data
// @Tags Reference
// @Accept json
// @Produce json
// @param id path int true "Kabkota ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /ref/kabkota/{id} [get]
func GetKabkota(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Kabkota{Id: uint(id)}

	result := database.DB.Preload(clause.Associations).Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting a record",
		Data:    record,
	})
}

// UpdateKabkota Update a Kabkota
// @Summary Update a Kabkota
// @Description Update a Kabkota
// @Tags Reference
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Kabkota ID"
// @param data body models.Kabkota true "Kabkota data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /ref/kabkota/{id} [put]
func UpdateKabkota(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Kabkota{Id: uint(id)}

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	var oldData models.Kabkota
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Model(&record).Preload(clause.Associations).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "kabkota", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteKabkota Delete a Kabkota
// @Summary Delete a Kabkota
// @Description Delete a Kabkota
// @Tags Reference
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Kabkota ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /ref/kabkota/{id} [delete]
func DeleteKabkota(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Kabkota{Id: uint(id)}

	var oldData models.Kabkota
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "kabkota", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Kabkota record deleted",
		Data:    record,
	})
}
