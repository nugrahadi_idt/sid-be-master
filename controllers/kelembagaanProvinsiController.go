package controllers

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
	"math"
	"net/url"
	"os"
	"sidat-amws/database"
	"sidat-amws/models"
	"sidat-amws/models/reqresp"
	"sidat-amws/util"
	"strconv"
	"strings"
)

// AllKelembagaanProvinsi KelembagaanProvinsi list
// @Summary KelembagaanProvinsi list
// @Description KelembagaanProvinsi list
// @Tags Statistik Kelembagaan
// @Accept json
// @Produce json
// @param provinsi_id query int false "Provinsi ID"
// @param bentuk_kelembagaan_id query int false "Bentuk Kelembagaan ID"
// @param nomenklatur query string false "Nomenklatur"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /kelembagaan-provinsi [get]
func AllKelembagaanProvinsi(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	provinsiId, _ := strconv.Atoi(c.Query("provinsi_id", "0"))
	bentukKelembagaanId, _ := strconv.Atoi(c.Query("bentuk_kelembagaan_id", "0"))
	nomenklatur := c.Query("nomenklatur", "")
	sortStr := c.Query("sort", "")

	var records []models.KelembagaanProvinsi

	baseQ := database.DB.Preload(clause.Associations).Offset(offset).Limit(limit)
	if provinsiId > 0 {
		baseQ = baseQ.Where("provinsi_id = ?", provinsiId)
	}

	if bentukKelembagaanId > 0 {
		baseQ = baseQ.Where("bentuk_kelembagaan_id = ?", bentukKelembagaanId)
	}

	if nomenklatur != "" {
		baseQ = baseQ.Where("UPPER(nomenklatur) LIKE ?", "%"+strings.ToUpper(nomenklatur)+"%")
	}

	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	//execute query
	baseQ.Find(&records)

	baseQ.Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       records,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateKelembagaanProvinsi Create a KelembagaanProvinsi
// @Summary Create a KelembagaanProvinsi
// @Description Create a KelembagaanProvinsi
// @Tags Statistik Kelembagaan
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param data body models.KelembagaanProvinsi true "KelembagaanProvinsi data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /kelembagaan-provinsi [post]
func CreateKelembagaanProvinsi(c *fiber.Ctx) error {
	var record models.KelembagaanProvinsi

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Preload(clause.Associations).Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "kelembagaan_provinsi", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetKelembagaanProvinsi Get single KelembagaanProvinsi data
// @Summary Get single KelembagaanProvinsi data
// @Description Get single KelembagaanProvinsi data
// @Tags Statistik Kelembagaan
// @Accept json
// @Produce json
// @param id path int true "KelembagaanProvinsi ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /kelembagaan-provinsi/{id} [get]
func GetKelembagaanProvinsi(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.KelembagaanProvinsi{Id: uint(id)}

	result := database.DB.Preload(clause.Associations).Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting a record",
		Data:    record,
	})
}

// UpdateKelembagaanProvinsi Update a KelembagaanProvinsi
// @Summary Update a KelembagaanProvinsi
// @Description Update a KelembagaanProvinsi
// @Tags Statistik Kelembagaan
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "KelembagaanProvinsi ID"
// @param data body models.KelembagaanProvinsi true "KelembagaanProvinsi data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /kelembagaan-provinsi/{id} [put]
func UpdateKelembagaanProvinsi(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.KelembagaanProvinsi{Id: uint(id)}

	if err := c.BodyParser(&record); err != nil {
		return err
	}


	var oldData models.KelembagaanProvinsi
	database.DB.Where("id = ?", id).First(&oldData)


	result := database.DB.Model(&record).Preload(clause.Associations).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "kelembagaan_provinsi", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteKelembagaanProvinsi Delete a KelembagaanProvinsi
// @Summary Delete a KelembagaanProvinsi
// @Description Delete a KelembagaanProvinsi
// @Tags Statistik Kelembagaan
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "KelembagaanProvinsi ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /kelembagaan-provinsi/{id} [delete]
func DeleteKelembagaanProvinsi(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.KelembagaanProvinsi{Id: uint(id)}

	var oldData models.KelembagaanProvinsi
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// delete uploaded file
	_ = os.Remove("./public/" + record.PerdaPembentukanPath)
	_ = os.Remove("./public/" + record.StrukturOrganisasiPath)
	_ = os.Remove("./public/" + record.NamaPersonilPath)
	//if err != nil {
	//	return c.Status(fiber.StatusInternalServerError).JSON(err)
	//}

	// send to log
	util.SendToAudit(c, "kelembagaan_provinsi", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "KelembagaanProvinsi record deleted",
		Data:    record,
	})
}

// GetStatsKelembagaanProvinsi Statistik KelembagaanProvinsi
// @Summary Statistik KelembagaanProvinsi
// @Description Statistik KelembagaanProvinsi
// @Tags Statistik Kelembagaan
// @Accept json
// @Produce json
// @param provinsi_id query int false "Provinsi ID"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.SuccessResponse
// @Router /kelembagaan-provinsi/stats [get]
func GetStatsKelembagaanProvinsi(c *fiber.Ctx) error {
	provinsiId, _ := strconv.Atoi(c.Query("provinsi_id", "0"))

	var records []reqresp.StatsResponse

	baseQ := database.DB.Table("kelembagaan_provinsi k").Select("bk.nomenklatur as name, count(*) as total")
	if provinsiId > 0 {
		baseQ = baseQ.Where("k.provinsi_id = ?", provinsiId)
	}
	baseQ = baseQ.Joins("join bentuk_kelembagaan bk on bk.id = k.bentuk_kelembagaan_id").Group("bk.name")

	//execute query
	baseQ.Find(&records)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success calculating KelembagaanProvinsi statistic",
		Data:    records,
	})
}
