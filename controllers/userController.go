package controllers

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
	"math"
	"net/url"
	"sidat-amws/database"
	"sidat-amws/models"
	"sidat-amws/models/reqresp"
	"sidat-amws/util"
	"strconv"
	"strings"
)

// AllUsers User list
// @Summary User list
// @Description User list
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param role_id query int false "Role ID"
// @param direktorat_id query int false "Direktorat ID"
// @param first_name query string false "First name"
// @param last_name query string false "Last name"
// @param email query string false "Email"
// @param nip query string false "NIP"
// @param nik query string false "NIK"
// @param no_telp query string false "No. Telp."
// @param is_active query bool false "active user"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /users [get]
func AllUsers(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	var users []models.User

	baseQ := database.DB.Preload(clause.Associations)

	// get params
	firstName := c.Query("first_name", "")
	lastName := c.Query("last_name", "")
	email := c.Query("email", "")
	nip := c.Query("nip", "")
	nik := c.Query("nik", "")
	noTelp := c.Query("no_telp", "")
	isActive := c.Query("is_active", "")
	roleId := c.Query("role_id", "")
	direktoratId := c.Query("direktorat_id", "")
	sortStr := c.Query("sort", "")

	if firstName != "" {
		baseQ = baseQ.Where("UPPER(first_name) LIKE ?", "%"+strings.ToUpper(firstName)+"%")
	}

	if lastName != "" {
		baseQ = baseQ.Where("UPPER(last_name) LIKE ?", "%"+strings.ToUpper(lastName)+"%")
	}
	if email != "" {
		baseQ = baseQ.Where("UPPER(email) LIKE ?", "%"+strings.ToUpper(email)+"%")
	}
	if nip != "" {
		baseQ = baseQ.Where("UPPER(nip) LIKE ?", "%"+strings.ToUpper(nip)+"%")
	}
	if nik != "" {
		baseQ = baseQ.Where("UPPER(nik) LIKE ?", "%"+strings.ToUpper(nik)+"%")
	}
	if noTelp != "" {
		baseQ = baseQ.Where("UPPER(no_telp) LIKE ?", "%"+strings.ToUpper(noTelp)+"%")
	}
	if isActive != "" {
		baseQ = baseQ.Where("is_active = ?", isActive)
	}
	if roleId != "" {
		baseQ = baseQ.Where("role_id = ?", roleId)
	}
	if direktoratId != "" {
		baseQ = baseQ.Where("direktorat_id = ?", direktoratId)
	}

	// sortir
	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	baseQ.Offset(offset).Limit(limit).Find(&users)

	baseQ.Model(&models.User{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       users,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateUser Create a user
// @Summary Create a user
// @Description Create a user
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param user body models.User true "User data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /users [post]
func CreateUser(c *fiber.Ctx) error {
	var record models.User

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	record.SetPassword(record.Email)

	//if len(record.Nik) <= 0 {
	//	record.Nik = record.Email
	//}

	if len(record.Nip) <= 0 {
		record.Nip = record.Email
	}

	result := database.DB.Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "user", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetUser Get single user data
// @Summary Get single user data
// @Description Get single user data
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "User ID"
// @Success 200 {object} models.User
// @Error 404 {object} reqresp.ErrorResponse
// @Router /users/{id} [get]
func GetUser(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	user := models.User{Id: uint(id)}

	result := database.DB.Preload("Role").Preload("Direktorat").Find(&user)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(user)
}

// GetUserByEmail Get User Data by Email
// @Summary Get User Data by Email
// @Description Get User Data by Email
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param email query string true "User email"
// @Success 200 {object} models.User
// @Error 404 {object} reqresp.ErrorResponse
// @Router /users/by-email [get]
func GetUserByEmail(c *fiber.Ctx) error {
	email := c.Query("email")

	user := models.User{Email: email}

	result := database.DB.Preload("Role").Preload("Direktorat").Find(&user)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(user)
}

// UpdateUser Update a user
// @Summary Update a user
// @Description Update a user
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "User ID"
// @param user body models.User true "User data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /users/{id} [put]
func UpdateUser(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.User{Id: uint(id)}

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	var oldData models.User
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Preload("Role").Preload("Direktorat").Model(&record).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	// send to log
	util.SendToAudit(c, "user", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteUser Delete a user
// @Summary Delete a user
// @Description Delete a user
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "User ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /users/{id} [delete]
func DeleteUser(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.User{Id: uint(id)}

	var oldData models.User
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record",
		})
	}

	// send to log
	util.SendToAudit(c, "user", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "record deleted",
		Data:    record,
	})
}
